-- Parameters:
--     destination_table             Database and table in which to insert data
--     month                         Data interval month (e.g. 2024-04); this will insert rows
--                                   for new editors who registered two months before (e.g. 
--                                   in 2024-02) so that the data interval will roughly
--                                   contain their second month after registration 
--     mediawiki_history_snapshot    Latest available snapshot

SET registration_month = TO_DATE('${month}') - INTERVAL 2 MONTH
;

-- mediawiki_history uses string timestamps, but native timestamps work together smoothly
-- with them.
SET registration_month_start = TO_TIMESTAMP(${registration_month})
;

SET registration_month_end = ${registration_month_start} + INTERVAL 1 MONTH
;

-- Delete any data that will be regenerated to avoid duplicates
DELETE FROM ${destination_table}
WHERE registration_month = ${registration_month}
;

WITH month_1 AS (
    SELECT
        event_user_text AS user_name,
        event_user_id AS user_local_id,
        wiki_db AS wiki,
        COUNT(*) AS edits
    FROM wmf.mediawiki_history
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
        AND snapshot = '${mediawiki_history_snapshot}'
        AND NOT event_user_is_created_by_system
        AND event_user_creation_timestamp >= ${registration_month_start}
        AND event_user_creation_timestamp < ${registration_month_end}
        AND event_timestamp < (event_user_creation_timestamp + INTERVAL 30 DAYS)
    GROUP BY
        event_user_text,
        event_user_id,
        wiki_db
), month_2 AS (
    SELECT
        event_user_text AS user_name,
        event_user_id AS user_local_id,
        wiki_db AS wiki,
        COUNT(*) AS edits
    FROM wmf.mediawiki_history
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
        AND snapshot = '${mediawiki_history_snapshot}'
        AND NOT event_user_is_created_by_system
        AND event_user_creation_timestamp >= ${registration_month_start}
        AND event_user_creation_timestamp < ${registration_month_end}
        AND event_timestamp >= (event_user_creation_timestamp + INTERVAL 30 DAYS)
        AND event_timestamp < (event_user_creation_timestamp + INTERVAL 60 DAYS)
    GROUP BY
        event_user_text,
        event_user_id,
        wiki_db
)
INSERT INTO ${destination_table}
-- The ideal file size for this table probably has many months of data per file.
-- We can't get that with a monthly insert, but we can at least use the COALESCE
-- hint to ensure we only output a single file for each month.
SELECT /*+ COALESCE(1) */
    ${registration_month} AS registration_month,
    user_name,
    user_local_id,
    wiki,
    month_1.edits AS month_1_edits,
    COALESCE(month_2.edits, 0) AS month_2_edits
FROM month_1
LEFT JOIN month_2
    -- User_name is a sufficient join key since each user should only have one
    -- registration where NOT event_user_is_created_by_system, but this way the
    -- overlapping fields all get combined
    USING (
        user_name,
        user_local_id,
        wiki
    )
;
