These queries create and update a table with a row for each newly registered user who edited during the first 30 days after their registration. They are intended for use in an Airflow job.

The easiest way to test `update_new_editor.sql` is run it on the command line as follows (using the [default settings for Airflow-managed Spark operations](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/config/dag_default_args.py?ref_type=heads#L118), with an increased executor memory overhead):
```
spark3-sql -f update_new_editor.sql \
    --driver-cores 2 \
    --driver-memory 4g \
    --executor-cores 2 \
    --executor-memory 4g \
    --master yarn \
    --deploy-mode client \
    --queue default \
    --conf spark.dynamicAllocation.enabled=true \
    --conf spark.dynamicAllocation.maxExecutors=16 \
    --conf spark.shuffle.service.enabled=true \
    --conf spark.executor.memoryOverhead=2048 \
    --conf spark.yarn.maxAppAttempts=1 \
    -d destination_table={{table}} \
    -d month={{month}} \
    -d mediawiki_history_snapshot={{snapshot}}
```

In testing the May 2024 update, this command took 9 min, 31 s to generate and insert the new data.
