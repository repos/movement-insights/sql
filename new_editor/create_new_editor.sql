-- Parameters:
--     destination_table         -- Database and name under which the created
--                                  table will be registered
--     destination_hdfs_path     -- HDFS directory in which to place the
--                                  generated data

CREATE EXTERNAL TABLE IF NOT EXISTS ${destination_table} (
    registration_month          DATE,
    user_name                   STRING,
    user_local_id               BIGINT,
    wiki_id                     STRING,
    month_1_edit_count          BIGINT,
    month_2_edit_count          BIGINT
)
USING ICEBERG
PARTITIONED BY (registration_month)
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${destination_hdfs_path}'
;
