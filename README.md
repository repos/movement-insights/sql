# Movement Insights SQL
SQL code for simple extract-transform-load data pipelines which do not require Python code. The pipelines themselves are configured in [the Airflow DAGs repo](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags).
