-- Parameters:
--     destination_table         -- Database and name under which the created
--                                  table will be registered
--     destination_hdfs_path     -- HDFS directory in which to place the
--                                  generated data

-- Does not use IF NOT EXISTS to avoid confusion when re-running the command
-- with an updated definition silently fails to change the table because it
-- already exists. Explicit is better than implicit.
CREATE EXTERNAL TABLE ${destination_table} (
    `month`                                DATE,
    `wiki_id`                              STRING,
    `project_family`                       STRING,
    `active_editor_count`                  BIGINT,
    `new_active_editor_count`              BIGINT,
    `returning_active_editor_count`        BIGINT
)
USING ICEBERG
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${destination_hdfs_path}'
;