SET month = DATE '${month}';

SET active_editor_count = COUNT(*) AS active_editor_count
;

SET new_active_editor_count =
    COUNT_IF(TRUNC(user_global_registration_dt, 'MM') = ${month})
    AS new_active_editor_count
;

SET returning_active_editor_count =
    COUNT(*) - COUNT_IF(TRUNC(user_global_registration_dt, 'MM') = ${month})
    AS returning_active_editor_count
;

-- Delete any data that will be regenerated to avoid duplicates
DELETE FROM ${destination_table}
WHERE month = ${month}
;

-- PART 1: Wiki-level active editors
INSERT INTO ${destination_table}
SELECT
    ${month} AS month,
    wiki_id,
    database_group AS project_family,
    ${active_editor_count},
    ${new_active_editor_count},
    ${returning_active_editor_count}
FROM wmf_contributors.editor_month
LEFT JOIN canonical_data.wikis
ON wiki_id = database_code
WHERE
    month = ${month}
    AND content_edit_count >= 5
    AND NOT user_is_bot
    AND NOT user_is_ip
    -- It isn't necessary to filter to "countable" wikis only because
    -- that happens during the generation of editor_month
GROUP BY
    wiki_id,
    database_group
;

-- PART 2: Project family–level active editors
WITH project_family_edits AS (
    SELECT
        database_group AS project_family,
        SUM(content_edit_count) AS content_edit_count,
        MAX(user_is_bot) AS user_is_bot,
        MIN(user_global_registration_dt) AS user_global_registration_dt
    FROM wmf_contributors.editor_month
    LEFT JOIN canonical_data.wikis
    ON wiki_id = database_code
    WHERE
        month = ${month}
        AND NOT user_is_ip
    GROUP BY
        database_group,
        user_name
)
INSERT INTO ${destination_table}
SELECT
    ${month} AS month,
    'All' as wiki_id,
    project_family,
    ${active_editor_count},
    ${new_active_editor_count},
    ${returning_active_editor_count}
FROM project_family_edits
WHERE
    content_edit_count >= 5
    AND NOT user_is_bot
GROUP BY project_family
;

-- PART 3: Global active editors
WITH global_edits AS (
    SELECT
        SUM(content_edit_count) AS content_edit_count,
        MAX(user_is_bot) AS user_is_bot,
        MIN(user_global_registration_dt) AS user_global_registration_dt
    FROM wmf_contributors.editor_month
    WHERE
        month = ${month}
        AND NOT user_is_ip
    GROUP BY user_name
)
INSERT INTO ${destination_table}
SELECT
    ${month} AS month,
    'All' as wiki_id,
    'All' as project_family,
    ${active_editor_count},
    ${new_active_editor_count},
    ${returning_active_editor_count}
FROM global_edits
WHERE
    content_edit_count >= 5
    AND NOT user_is_bot
;
