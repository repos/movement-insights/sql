-- Parameters:
--     destination_table         -- Database and name under which the created
--                                  table will be registered
--     destination_hdfs_path     -- HDFS directory in which to place the
--                                  generated data

CREATE EXTERNAL TABLE IF NOT EXISTS ${destination_table} (
    month                                DATE,
    wiki_id                              STRING,
    user_name                            STRING,
    user_local_id                        BIGINT,
    user_global_registration_dt          TIMESTAMP,
    user_is_ip                           BOOLEAN,
    user_is_temporary                    BOOLEAN,
    user_is_bot                          BOOLEAN,
    edit_count                           BIGINT,
    content_edit_count                   BIGINT,
    mobile_edit_count                    BIGINT
)
USING ICEBERG
PARTITIONED BY (month)
TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)
LOCATION '${destination_hdfs_path}'
;
