import subprocess

import pandas as pd

months = pd.date_range(start="2001-01-01", end="2024-06-01", freq="MS").strftime("%Y-%m")

for month in months:
    time = pd.Timestamp.now().strftime("%Y-%m-%d %H:%M:%S")

    print(
        f"[{time}] INFO: Starting update_editor_month.sql for {month}.",
        # Flushing the stdout buffer ensures this statement is printed immediately
        flush=True
    )

    command = (
        "sudo -u analytics-product kerberos-run-command analytics-product "
        "spark3-sql -f update_editor_month.sql --master yarn "
        "--executor-memory 8g "
        "--executor-cores 4 "
        "--driver-memory 2g "
        "--conf spark.dynamicAllocation.maxExecutors=32 "
        "--conf spark.sql.shuffle.partitions=256 "
        "--conf spark.executor.memoryOverhead=2048m "
        "-d destination_table=wmf_contributors.editor_month "
        f"-d month={month} "
        "-d mediawiki_history_snapshot=2024-06"
    )

    subprocess.run(command, shell=True)
