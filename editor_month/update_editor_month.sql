-- Parameters:
--     destination_table             Database and table in which to insert data
--     month                         Data interval month in yyyy-MM format (e.g. 2024-04)
--     mediawiki_history_snapshot    Latest available snapshot


-- mediawiki_history uses string timestamps, but native timestamps are easier to work with
-- compare smoothly with string timestamps.
SET month_start = TO_TIMESTAMP('${month}')
;

SET next_month_start = ${month_start} + INTERVAL 1 MONTH
;

-- Delete any data that will be regenerated to avoid duplicates
DELETE FROM ${destination_table}
WHERE month = TO_DATE(${month_start})
;

WITH revision AS (
    SELECT
        wiki_db AS wiki_id,
        event_user_id AS user_local_id,
        event_user_text AS user_name,
        -- Pages can be moved in and out of content namespaces (usually
        -- when drafts are promoted to articles or articles demoted to 
        -- drafts), so there can be a difference between current and historical
        -- content-namespace status. Using historical eliminates this source
        -- of after-the-fact shift in our metrics.
        page_namespace_is_content_historical AS page_is_content,
        (
            ARRAY_CONTAINS(revision_tags, 'mobile edit')
            -- There are about 600 k mobile app edits that did not
            -- get the general mobile edit tag (T141667)
            OR ARRAY_CONTAINS(revision_tags, 'mobile app edit')
        ) AS interface_is_mobile,
        size(event_user_is_bot_by_historical) > 0 AS user_is_bot,
        event_user_is_anonymous AS user_is_ip,
        event_user_is_temporary AS user_is_temporary
    FROM wmf.mediawiki_history
    LEFT JOIN canonical_data.wikis
    ON wiki_db = database_code
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
        AND snapshot = '${mediawiki_history_snapshot}'
        AND database_group IN (
            'commons', 'incubator', 'foundation', 'mediawiki',
            'meta', 'sources', 'species', 'wikibooks',
            'wikidata', 'wikifunctions', 'wikinews', 'wikipedia',
            'wikiquote', 'wikisource', 'wikiversity', 'wikivoyage',
            'wiktionary'
        )
        AND event_timestamp >= ${month_start}
        AND event_timestamp < ${next_month_start}
),
-- We have to do some contortions to figure out the global registration date
-- for this month's editors. Ideally it would be available directly from
-- mediawiki_history.
editor AS (
    SELECT DISTINCT user_name
    FROM revision
    -- IP users have no registration date, so we skip them up front
    WHERE NOT user_is_ip
),
editor_global_registration AS (
    SELECT
        event_user_text AS user_name,
        TO_TIMESTAMP(LEAST(
            -- Sometimes the user and logging tables have inconsistent
            -- registration times

            MIN(event_user_registration_timestamp),
            MIN(event_user_creation_timestamp),
            -- Users registered before ~2008 don't have registration times
            MIN(event_user_first_edit_timestamp)
        )) AS user_global_registration_dt
    FROM wmf.mediawiki_history
    WHERE
        event_entity = 'revision'
        AND event_type = 'create'
        AND snapshot = '${mediawiki_history_snapshot}'
        -- IP users have no registration date, so we skip them up front
        AND NOT event_user_is_anonymous
        -- If we do a left join between editors and mediawiki_history,
        -- Spark 3.1.2 attempts to first aggregate the global registration dates
        -- for all editors in mediawiki_history and only afterward filter to
        -- just this month's editors, which causes the job to fail even with
        -- settings for an extra-large jobs.
        --
        -- Using the IN operator instead ensures that Spark filters to this
        -- month's editors during the initial scan of mediawiki_history, before
        -- aggregating the global registration dates, which performs vastly
        -- better. 
        AND event_user_text IN (SELECT * FROM editor)
        -- When backfilling, there's no need to look at the future to find
        -- users' first events
        AND event_timestamp < ${next_month_start}
    GROUP BY event_user_text
),
editor_month AS (
    SELECT
        TO_DATE(${month_start}) AS month,
        wiki_id,
        user_name,
        user_local_id,
        -- All of a user's joined registrations dates will be the same, so we don't
        -- care which one we get
        FIRST(user_global_registration_dt) AS user_global_registration_dt,
        user_is_ip,
        user_is_temporary,
        -- Bot status can be mixed within a month. If it is, we generally 
        -- err on the side of tagging and excluding the user.
        ANY(user_is_bot) AS user_is_bot,
        COUNT(*) AS edit_count,
        COUNT_IF(page_is_content) AS content_edit_count,
        COUNT_IF(interface_is_mobile) AS mobile_edit_count
    FROM revision
    LEFT JOIN editor_global_registration
    USING (user_name)
    GROUP BY
        TO_DATE(${month_start}),
        wiki_id,
        user_local_id,
        user_name,
        user_is_ip,
        user_is_temporary
)
INSERT INTO ${destination_table}
-- The ideal file size for this table is probably 3-4 months of data per file.
-- We can't get that with a monthly insert, but we can at least use the REPARTITION
-- hint to ensure we only output a single file for each month.
--
-- It's important that we use REPARTITION(1) rather than COALESCE(1), as the latter
-- causes Spark to attempt to do an huge amount of processing in a single task. For
-- more details, see https://stackoverflow.com/questions/44494656/.
SELECT /*+ REPARTITION(1) */
    *
FROM editor_month
;