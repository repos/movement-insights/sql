These commands create and update an editor-month table with a row for each user who edited at a given wiki during a given month. They are intended for use in an Airflow job.

The easiest way to test changes is on the command line.

You can run `create_editor_month.sql` as follows:
```
spark3-sql -f create_editor_month.sql \
    -d destination_table={{table}} \
    -d destination_hdfs_path={{hdfs path}}
```

You can run `update_editor_month.sql` as follows, using the ["regular" Spark settings](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs) since it requires a lot of processing power. When testing with April 2024 data, the command took about 7 mins to run.
```
spark3-sql -f update_editor_month.sql \
    --master yarn \
    --executor-memory 8g \
    --executor-cores 4 \
    --driver-memory 2g \
    --conf spark.dynamicAllocation.maxExecutors=64 \
    --conf spark.sql.shuffle.partitions=256 \
    -d destination_table={{table}} \
    -d month={{yyyy-MM}} \
    -d mediawiki_history_snapshot={{snapshot}}
```
